#include "testing.h"
#include <stdlib.h>
#include <stdio.h>

void pruebas_heap_alumno(void);

/* ******************************************************************
 *                        PROGRAMA PRINCIPAL
 * *****************************************************************/

int main(int argc, char *argv[])
{
    printf("\n");
    printf("~~~ PRUEBAS ALUMNO ~~~\n\n");
    pruebas_heap_alumno();

    printf("CANTIDAD DE FALLAS: %i\n\n", failure_count());
    return failure_count() > 0;
}
