#define _GNU_SOURCE // para asprintf
#include <stdio.h>
#include <stdlib.h>
#include <stdio.h>
#include <stddef.h>
#include "testing.h"
#include "heap.h"

#define VERBOSE true
#define NOT_VERBOSE false

/*******************************************************************
 *                     FUNCIONES AUXILIARES
 ******************************************************************/

static int comparar_enteros(const void* valor_1, const void* valor_2){

    int a = *(int*)valor_1;
    int b = *(int*)valor_2;

	if(a > b){
		return 1;
	}
	else if(a < b){
		return -1;
	}
	return 0;
}

static int comparar_enteros_2(const void* valor_1, const void* valor_2){

    int a = *(int*)valor_1;
    int b = *(int*)valor_2;

	if(a > b){
		return -1;
	}
	else if(a < b){
		return 1;
	}
	return 0;
}

static bool arreglo_esta_ordenado(void** arr, size_t tamanio, cmp_func_t cmp){

	void* min = arr[0];
	for(size_t i = 1; i < tamanio; i++){
		if(cmp(min, arr[i]) > 0){
			return false;
		}
		min = arr[i];
	}
	return true;

}

static int** arr_copy(const int* original, size_t n){

	int** copia = malloc(n * sizeof(int*));
	if(copia == NULL){
		return NULL;
	}

	for(size_t i = 0; i < n; i++){
		int* ptr = malloc(sizeof(int));
		if(ptr == NULL) {
			for(size_t j = 0; j < i; j++){
				free(copia[j]);
			}
			free(copia);
			return NULL;
		}

		*ptr = original[i];
		copia[i] = ptr;
	}

	return copia;
}

static int** arr_rand(size_t n){

	int** arr = malloc(n * sizeof(int*));
	if(arr == NULL){
		return NULL;
	}

	for(int i = 0; i < n; i++){
		int* ptr = malloc(sizeof(int));
		if(ptr == NULL){
			for(size_t j = 0; j < i; j++){
				free(arr[j]);
			}
			free(arr);
			return NULL;
		}

		int aleatorio = rand();
		int moneda = rand() % 2;	// Mitad negativos, mitad positivos.
		if(moneda == 0){
			aleatorio *= -1;
		}

		*ptr = aleatorio;
		arr[i] = ptr;
	}

	return arr;
}

static void arr_print(int **elementos, size_t n, char *ultimo){

	printf("[");
	for(size_t i = 0; i <= n - 1; i++){
		int* dato = elementos[i];
		printf("%d", *dato);
		if(i != n - 1){
			printf(" ");
		}
	}
	printf("]");
	printf("%s", ultimo);
}

static void arr_free(int** arr, size_t tamanio){
	for(size_t i = 0; i < tamanio; i++){
		free(arr[i]);
	}
	free(arr);
}

/*******************************************************************
 *                        PRUEBAS HEAP SORT
 ******************************************************************/

static void prueba_heap_sort(char* msg, const int arreglo[], size_t tamanio, cmp_func_t cmp, bool verbose){

	int** original = malloc(tamanio * sizeof(int*));
	if(original == NULL){
		return;
	}

	int** ordenado = malloc(tamanio * sizeof(int*));
	if(ordenado == NULL){
		free(original);
		return;
	}

	for(size_t i = 0; i < tamanio; i++){
		int* ptr = malloc(sizeof(int));
		if(ptr == NULL) {
			for(size_t j = 0; j < i; j++){
				free(original[j]);
			}
			free(original);
			free(ordenado);
			return;
		}
		*ptr = arreglo[i];
		original[i] = ptr;
		ordenado[i] = ptr;
	}


	heap_sort((void**) ordenado, tamanio, cmp);
	print_test(msg, arreglo_esta_ordenado((void**)ordenado, tamanio, cmp));

	if(verbose){
		arr_print(original, tamanio, "");
		printf(" --> ");
		arr_print(ordenado, tamanio, "");
		printf("\n\n");
	}

	for(size_t i = 0; i < tamanio; i++){
		free(original[i]);
	}
	free(original);
	free(ordenado);
}


static void pruebas_heap_sort(){

	printf("PRUEBAS HEAPSORT\n");

	int arr1[] = {4,1,3,2,16,9,10,14,8,7};
	size_t n1 = 10;
	prueba_heap_sort("Prueba heapsort de menor a mayor arr1", arr1, n1, comparar_enteros, NOT_VERBOSE);
	prueba_heap_sort("Prueba heapsort de mayor a menor arr1 ", arr1, n1, comparar_enteros_2, NOT_VERBOSE);

	int arr2[] = {55,13,2,25,7,17,20,8,4};
	size_t n2 = 9;
	prueba_heap_sort("Prueba heapsort de menor a mayor arr2", arr2, n2, comparar_enteros, NOT_VERBOSE);
	prueba_heap_sort("Prueba heapsort de mayor a menor arr2 ", arr2, n2, comparar_enteros_2, NOT_VERBOSE);

	int arr3[] = {50,1,2,3,30,10,80};
	size_t n3 = 7;
	prueba_heap_sort("Prueba heapsort de menor a mayor arr3", arr3, n3, comparar_enteros, NOT_VERBOSE);
	prueba_heap_sort("Prueba heapsort de mayor a menor arr3", arr3, n3, comparar_enteros_2, NOT_VERBOSE);

	printf("\n");
}

/*******************************************************************
 *                      PRUEBAS HEAP DE MÁXIMO
 ******************************************************************/

static void prueba_crear_arr_i(heap_t *heap, int **ordenado, size_t i, cmp_func_t cmp, bool *heap_ok, bool verbose){

	bool ver_max_ok = true;
	bool desencolar_ok = true;
	char* msg = NULL;

	int* arr_val = ordenado[i];

	int* heap_max = heap_ver_max(heap);
	ver_max_ok = cmp(heap_max, arr_val) == 0;

	if(heap_ok && !ver_max_ok){
		*heap_ok = false;
	}
	if(verbose){
		if(asprintf(&msg, "Prueba ver_max es %d", *arr_val)){
            print_test(msg, ver_max_ok);
            free(msg);
		}
		else{
            print_test("Prueba ver max", ver_max_ok);
		}

	}

	int* desencolado = heap_desencolar(heap);
	desencolar_ok = cmp(desencolado, arr_val) == 0;

	if(heap_ok && !desencolar_ok){
		*heap_ok = false;
	}
	if(verbose){
		if(asprintf(&msg, "Prueba desencolar es %d", *arr_val)){
			print_test(msg, desencolar_ok);
			free(msg);
		}
		else{
			print_test("Prueba desencolar", desencolar_ok);
		}

	}

	size_t cantidad = heap_cantidad(heap);
	bool cantidad_ok = cantidad == i;

	if(heap_ok && !cantidad_ok){
		*heap_ok = false;
	}
	if(verbose){
		if(asprintf(&msg, "Prueba cantidad es %zu", i)){
			print_test(msg, cantidad_ok);
			free(msg);
		}
		else{
			print_test("Prueba cantidad", cantidad_ok);
		}

	}

	bool esta_vacio = heap_esta_vacio(heap);
	bool esta_vacio_ok = true;

	// ya desencolé el último;
	if(i == 0 && !esta_vacio){
		esta_vacio_ok = false;
	}

	if(heap_ok && !esta_vacio_ok){
		*heap_ok = false;
	}
	if(verbose){
		if(asprintf(&msg, "Prueba esta_vacio es %s", cantidad == 0 ? "true": "false")){
			print_test(msg, esta_vacio_ok);
			free(msg);
		}
		else{
			print_test("Prueba esta_vacio", esta_vacio_ok);
		}

	}
}

static void prueba_crear_arr(int arreglo[], size_t tamanio, cmp_func_t cmp, bool verbose){

	bool heap_ok = true;

	int** original = arr_copy(arreglo, tamanio);
	if(original == NULL){
		return;
	}

	heap_t* heap = heap_crear_arr((void**)original, tamanio, cmp);
	if(heap == NULL){
		free(original);
		print_test("Prueba crear_heap_arr", false);
		return;
	}

	int** ordenado = arr_copy(arreglo, tamanio);
	if(ordenado == NULL){
		free(original);
		heap_destruir(heap, free);
		return;
	}
	heap_sort((void**)ordenado, tamanio, cmp);

	printf("arr = ");
	arr_print(original, tamanio, ", ");
	printf("n = %zu\n", tamanio);

	printf("ord = ");
	arr_print(ordenado, tamanio, ", ");
	printf("n = %zu\n", tamanio);

	if(verbose){
		print_test("Prueba crear_heap_arr", heap != NULL);
	}

	for(size_t i = tamanio - 1; i >= 1; i--){
		prueba_crear_arr_i(heap, ordenado, i, cmp, &heap_ok, verbose);
	}
	prueba_crear_arr_i(heap, ordenado, 0, cmp, &heap_ok, verbose);

	if(!verbose){
		print_test("Prueba heap_crear_arr (ver_max, desencolar, cantidad y esta_vacio)", heap_ok);
	}

	arr_free(original, tamanio);
	arr_free(ordenado, tamanio);
	heap_destruir(heap, NULL);
}

static void pruebas_crear_arr(){

	printf("PRUEBAS heap_crear_arr\n");

	int arr1[] = {4,1,3,2,16,9,10,14,8,7};
	size_t n1 = 10;
	prueba_crear_arr(arr1, n1, comparar_enteros, VERBOSE);

	printf("\n");
}


static void pruebas_heap_vacio(){

	printf("PRUEBAS heap vacio\n");

	heap_t* heap = heap_crear(NULL);
	print_test("Prueba crear heap vacío", heap != NULL);
	print_test("Prueba esta_vacio es true", heap_esta_vacio(heap));
	print_test("Prueba cantidad es cero",heap_cantidad(heap) == 0);
	print_test("Prueba ver_max es NULL", heap_ver_max(heap) == NULL);
	print_test("Prueba desencolar es NULL",heap_desencolar(heap) == NULL);
	print_test("Prueba desencolar es NULL",heap_desencolar(heap) == NULL);
	heap_destruir(heap,NULL);

	printf("\n");
}

static void pruebas_algunos_elementos(){

	printf("PRUEBAS COMPORTAMIENTO GENERAL\n");

	heap_t* heap = heap_crear(comparar_enteros);
	int cinco = 5;
	int dos = 2;
	int siete = 7;
	int diez = 10;
	int* valor_1 = &cinco;
	int* valor_2 = &dos;
	int* valor_3 = &siete;
	int* valor_4 = &diez;

	print_test("Prueba crear heap vacío", heap != NULL);
	print_test("Prueba encolar valor 1", heap_encolar(heap, (void*)valor_1));
	print_test("Prueba esta_vacio es false", !heap_esta_vacio(heap));
	print_test("Prueba cantidad es 1", heap_cantidad(heap) == 1);

	print_test("Prueba desencolar es valor1", *((int*)heap_desencolar(heap)) == *valor_1);
	print_test("Prueba esta_vacio es true", heap_esta_vacio(heap));
	print_test("Prueba cantidad es 0", heap_cantidad(heap) == 0);
	print_test("Prieba desencolar es NULL", heap_desencolar(heap) == NULL);
	print_test("Prueba ver_max es NULL", heap_ver_max(heap) == NULL);

	print_test("Prueba encolar valor1", heap_encolar(heap, (void*)valor_1));
	print_test("Prueba encolar valor2", heap_encolar(heap, (void*)valor_2));
	print_test("Prueba desencolar es valor1", *((int*)heap_desencolar(heap)) == *valor_1);
	print_test("Prueba cantidad es 1", heap_cantidad(heap) == 1);
	print_test("Prueba encolar valor1", heap_encolar(heap, valor_1));
	print_test("Prueba ver_max es valor1", *((int*)heap_ver_max(heap)) == *valor_1);
	print_test("Prueba cantidad es 2", heap_cantidad(heap) == 2);
	print_test("Prueba desencolar es valor1", *((int*)heap_desencolar(heap)) == *valor_1);
	print_test("Prueba esta_vacio es false", !heap_esta_vacio(heap));
	print_test("Prueba cantidad es 1", heap_cantidad(heap) == 1);
	print_test("Prueba encolar valor3", heap_encolar(heap, (void*)valor_3));
	print_test("Prueba ver_max es valor3", *((int*)heap_ver_max(heap)) == *valor_3);
	print_test("Prueba encolar valor2", heap_encolar(heap, (void*)valor_2));
	print_test("Prueba ver_max es valor3", *((int*)heap_ver_max(heap)) == *valor_3);
	print_test("Prueba encolar valor 4", heap_encolar(heap, (void*)valor_4));
	print_test("Prueba ver_max es valor4", *((int*)heap_ver_max(heap)) == *valor_4);
	print_test("Prueba ver_max es valor4", *((int*)heap_ver_max(heap)) == *valor_4);
	print_test("Prueba desencolar es valor4", *((int*)heap_desencolar(heap)) == *valor_4);
	print_test("Prueba ver_max es valor3", *((int*)heap_ver_max(heap)) == *valor_3);
	print_test("Prueba ver_max es valor3", *((int*)heap_ver_max(heap)) == *valor_3);
	print_test("Prueba desencolar es valor3", *((int*)heap_desencolar(heap)) == *valor_3);
	print_test("Prueba ver_max es valor2", *((int*)heap_ver_max(heap)) == *valor_2);
	print_test("Prueba desencolar es valor2", *((int*)heap_desencolar(heap)) == *valor_2);
	print_test("Prueba desencolar es valor 2", *((int*)heap_desencolar(heap)) == *valor_2);
	print_test("Prueba esta_vacio es true", heap_esta_vacio(heap));
	print_test("Prueba cantidad es 0", heap_cantidad(heap) == 0);
	print_test("Prueba desencolar es NULL", heap_desencolar(heap) == NULL);

	heap_destruir(heap, NULL);

	printf("\n");
}

static void pruebas_volumen(size_t n){

	printf("PRUEBAS DE VOLUMEN\n");

	char* msg = NULL;

	heap_t* heap = heap_crear(comparar_enteros);

	int** arr = arr_rand(n);

	bool encolar_muchos_ok = true;
	for(size_t i = 0; i < n; i++){
		bool encolado = heap_encolar(heap, arr[i]);
		if(!encolado){
			encolar_muchos_ok= false;
			break;
		}
	}
	encolar_muchos_ok = encolar_muchos_ok && heap_cantidad(heap) == n;
	if(asprintf(&msg, "Prueba encolar %zu elementos", n)){
		print_test(msg, encolar_muchos_ok);
		free(msg);
	}
	else{
		print_test("Prueba encolar muchos elementos", encolar_muchos_ok);
	}


	heap_sort((void**)arr, n, comparar_enteros);
	size_t i = n - 1;
	bool ver_max_muchos_ok = true;
	bool desencolar_muchos_ok = true;
	while(!heap_esta_vacio(heap)){

		int* tope = heap_ver_max(heap);
		int* arr_val = arr[i];
		if(comparar_enteros(tope, arr_val) != 0){
			ver_max_muchos_ok = false;
			break;
		}

		int* desencolado = heap_desencolar(heap);
		if(comparar_enteros(desencolado, arr[i]) != 0){
			desencolar_muchos_ok = false;
			break;
		}
		i--;
	}
	if(asprintf(&msg, "Prueba desencolar %zu elementos", n)){
		print_test(msg, ver_max_muchos_ok && desencolar_muchos_ok);
		free(msg);
	}
	else{
		print_test("Prueba desencolar muchos elementos", ver_max_muchos_ok && desencolar_muchos_ok);
	}


	print_test("Prueba cantidad es 0", heap_cantidad(heap) == 0);
	print_test("Prueba esta_vacio es true", heap_esta_vacio(heap));
	print_test("Prueba ver_max es NULL", heap_ver_max(heap) == NULL);
	print_test("Prueba desencolar es NULL", heap_desencolar(heap) == NULL);

	arr_free(arr, n);
	heap_destruir(heap, NULL);

	printf("\n");
}

void pruebas_heap_alumno(){
	pruebas_heap_sort();
    pruebas_crear_arr();
	pruebas_heap_vacio();
	pruebas_algunos_elementos();
	pruebas_volumen(5000);
}