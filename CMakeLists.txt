cmake_minimum_required(VERSION 3.3)
project(heap)

set(CMAKE_C_FLAGS "${CMAKE_C_FLAGS} -g -std=c99 -Wall -Wconversion -Wtype-limits -Wbad-function-cast -Wshadow -Wpointer-arith -Wunreachable-code -pedantic -Werror")

set(SOURCE_FILES heap.c main.c heap_pruebas_alumno.c testing.c)
add_executable(abb ${SOURCE_FILES})