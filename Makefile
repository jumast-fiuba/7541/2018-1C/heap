CC = gcc
CFLAGS = -g -std=c99 -Wall -Wconversion -Wno-sign-conversion -Wbad-function-cast
CFLAGS += -Wshadow -Wpointer-arith -Wunreachable-code -Wformat=2 -Werror
CFLAGS += -Wunused-result
CGLAGS += -pedantic
VALFLAGS = --leak-check=full --track-origins=yes --show-reachable=yes
EXEC = pruebas
ZIPNAME = heap_entrega.zip

%.o: %.c %.h
	$(CC) $(CFLAGS) -c $<

HEAP_OBJECTS = heap.o heap_pruebas_alumno.o main.o testing.o
build: $(HEAP_OBJECTS) 
	$(CC) $(CFLAGS) -o $(EXEC) $(HEAP_OBJECTS)
	
run: build
	./$(EXEC)
	$(MAKE) clean
	
val: build
	valgrind $(VALFLAGS) ./$(EXEC)
	$(MAKE) clean

clean:
	rm -f *.o $(EXEC) $(ZIPNAME)

HEAP = heap.c heap.h

ENTREGA = $(HEAP)
ENTREGA += heap_pruebas_alumno.c
ENTREGA += Makefile

zip:
	zip $(ZIPNAME) $(ENTREGA)




