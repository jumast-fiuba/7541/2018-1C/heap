#include <stdlib.h>
#include "heap.h"
#define TAMANIO_INICIAL 100
#define CONSTANTE_REDIMENSION 2

#include <stdio.h>

/*******************************************************************
 *                DECLARACIÓN DE FUNCIONES AUILIARES
 ******************************************************************/

static void swap (void** x, void** y);
static size_t padre(size_t i);
static size_t hijo_derecho(size_t i);
static size_t hijo_izquierdo(size_t i);
static void upheap(void** arr, size_t i, cmp_func_t cmp);
static void downheap(void **vector, size_t i, size_t tam_heap, cmp_func_t cmp);
static void heapify(void **vector, size_t n, cmp_func_t cmp);

static bool heap_redimensionar(heap_t* heap, size_t tamanio_nuevo);
static bool heap_incrementar_cantidad(heap_t *heap);
static bool heap_decrementar_cantidad(heap_t* heap);

/*******************************************************************
 *                           HEAP SORT
 ******************************************************************/

void heap_sort(void* elementos[], size_t cant, cmp_func_t cmp){

	heapify(elementos, cant, cmp);

	for(size_t i = cant - 1; i >= 1; i--){
		swap(&elementos[0], &elementos[i]);
		downheap(elementos, 0, i, cmp);
	}
}

/*******************************************************************
 *                        HEAP: PRIMITIVAS
 ******************************************************************/

struct heap{
	void** datos;
	size_t tamanio;
	size_t cantidad;
	cmp_func_t cmp;
};

heap_t* heap_crear(cmp_func_t cmp){
	heap_t* heap = malloc(sizeof(heap_t));
	if(heap == NULL){
		return NULL;
	}

	void** datos = malloc(TAMANIO_INICIAL * sizeof(void*));
	if(datos == NULL){
		free(heap);
		return NULL;
	}

	heap->datos = datos;
    heap->tamanio = TAMANIO_INICIAL;
	heap->cantidad = 0;
	heap->cmp = cmp;

	return heap;
}

heap_t *heap_crear_arr(void *arreglo[], size_t n, cmp_func_t cmp){
	heap_t* heap = malloc(sizeof(heap_t));
	if(heap == NULL){
		return NULL;
	}

	size_t tamanio_inicial = n < TAMANIO_INICIAL ? TAMANIO_INICIAL : n * CONSTANTE_REDIMENSION;

	void** datos = malloc(tamanio_inicial * sizeof(void*));
	if(datos == NULL){
		free(heap);
		return NULL;
	}


	for(size_t i = 0; i < n; i++){
		datos[i] = arreglo[i];
	}
	heapify(datos, n, cmp);

	heap->datos = datos;
	heap->tamanio = tamanio_inicial;
	heap->cantidad = n;
	heap->cmp = cmp;

	return heap;
}

void heap_destruir(heap_t* heap, void destruir_elemento(void*)){

	for(size_t i = 0; i < heap_cantidad(heap); i++){
		void* dato = heap->datos[i];
		if(destruir_elemento != NULL){
			destruir_elemento(dato);
		}
	}

	free(heap->datos);
	free(heap);
}

size_t heap_cantidad(const heap_t* heap){
	return heap->cantidad;
}

bool heap_esta_vacio(const heap_t* heap){
	return heap->cantidad == 0;
}

void* heap_ver_max(const heap_t* heap){
	if(heap_esta_vacio(heap)){
		return NULL;
	}
	return heap->datos[0];
}

bool heap_encolar(heap_t* heap, void* elem){

	if(elem == NULL){
		return false;
	}

	heap_incrementar_cantidad(heap);
	heap->datos[heap->cantidad - 1] = elem;
	upheap(heap->datos, heap->cantidad - 1, heap->cmp);

	return true;
}

void* heap_desencolar(heap_t* heap){

	if(heap_esta_vacio(heap)){
		return NULL;
	}

	void* maximo = heap->datos[0];
	heap->datos[0] = heap->datos[heap->cantidad - 1];
	heap_decrementar_cantidad(heap);
	downheap(heap->datos, 0, heap->cantidad, heap->cmp);

	return maximo;
}

/*******************************************************************
 *                IMPLEMENTACIÓN DE FUNCIONES AUILIARES
 ******************************************************************/
static void swap (void** x, void** y) {
	void* aux = *x;
	*x = *y;
	*y = aux;
}

static void upheap(void** arr, size_t i, cmp_func_t cmp){
	if(i == 0){
		return;
	}

	size_t pos_padre = padre(i);
	if(cmp(arr[i], arr[pos_padre]) < 0){
		return;
	}

	swap(&arr[i], &arr[pos_padre]);
	upheap(arr, pos_padre, cmp);
}

static void downheap(void **vector, size_t i, size_t tam_heap, cmp_func_t cmp){
	size_t pos_max = i;

	size_t pos_hijo_izquierdo = hijo_izquierdo(i);
	if(pos_hijo_izquierdo < tam_heap && cmp(vector[pos_hijo_izquierdo] , vector[i]) > 0){
		pos_max = pos_hijo_izquierdo;
	}

	size_t pos_hijo_derecho = hijo_derecho(i);
	if(pos_hijo_derecho < tam_heap && cmp(vector[pos_hijo_derecho] , vector[pos_max]) > 0){
		pos_max = pos_hijo_derecho;
	}

	if(pos_max != i){
		swap(&vector[i], &vector[pos_max]);
		downheap(vector, pos_max, tam_heap, cmp);
	}
}

static void heapify(void **vector, size_t n, cmp_func_t cmp){
	for(size_t j = ((n - 1) / 2); j >= 1; j--){
		downheap(vector, j, n, cmp);
	}
	downheap(vector, 0, n, cmp);
}

static size_t padre(size_t i){
	if(i % 2 == 0){
		return (i / 2) -1;
	}
	return i / 2;
}

static size_t hijo_derecho(size_t i){
	return 2 * i + 2;
}

static size_t hijo_izquierdo(size_t i){
	return 2 * i + 1;
}

static bool heap_incrementar_cantidad(heap_t *heap){

	bool agrandar = heap->cantidad > 9 * (heap->tamanio / 10);
	if(agrandar){

		size_t tamanio_nuevo = heap->tamanio * CONSTANTE_REDIMENSION;
		bool redimension_ok = heap_redimensionar(heap, tamanio_nuevo);

		if(redimension_ok == false){
			return false;
		}
	}

	heap->cantidad++;
	return true;
}

static bool heap_decrementar_cantidad(heap_t* heap){

	heap->cantidad--;

	bool achicar = heap->cantidad < heap->tamanio / 2;
	if(achicar){

		size_t tamanio_nuevo = heap->tamanio / CONSTANTE_REDIMENSION;
		bool redimension_ok = heap_redimensionar(heap, tamanio_nuevo);

		if(redimension_ok == false){
			heap->cantidad++;
			return false;
		}
	}

	return true;
}

static bool heap_redimensionar(heap_t* heap, size_t tamanio_nuevo){

	void** datos_nuevo = realloc(heap->datos, tamanio_nuevo * sizeof(void*));
	if(datos_nuevo == NULL){
		return false;
	}

	heap->datos = datos_nuevo;
	heap->tamanio = tamanio_nuevo;

	return true;
}